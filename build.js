const sass = require('node-sass');
const fs = require('fs');

const result = sass.renderSync({
    file: './scss/_theme.scss'
});

if (fs.existsSync('./dist') === false) {
    fs.mkdirSync('./dist');
}

fs.writeFileSync('./dist/theme.css', result.css);